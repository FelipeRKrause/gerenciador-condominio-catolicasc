import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ContactProvider } from '../providers/contact/contact';
import { TipoProvider } from '../providers/tipo/tipo';
import { FornecedorProvider } from '../providers/fornecedor/fornecedor';
import { DespesaProvider } from '../providers/despesa/despesa';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyCOzfOzHpyxZdMZyHpYpIUTs_4K8kyF0Rw",
      authDomain: "gerenciadorcondominio.firebaseapp.com",
      databaseURL: "https://gerenciadorcondominio.firebaseio.com",
      projectId: "gerenciadorcondominio",
      storageBucket: "gerenciadorcondominio.appspot.com",
      messagingSenderId: "381657974879"
    }),
    AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ContactProvider,
    TipoProvider,
    FornecedorProvider,
    DespesaProvider
  ]
})
export class AppModule {}

