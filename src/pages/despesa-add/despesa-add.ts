import { DespesaProvider } from './../../providers/despesa/despesa';
import { TipoProvider } from './../../providers/tipo/tipo';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

@IonicPage()
@Component({
  selector: 'page-despesa-add',
  templateUrl: 'despesa-add.html',
})
export class DespesaAddPage {
  title: string;
  form: FormGroup;
  despesa: any;
  tipos: Observable<any>;

  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    private formBuilder: FormBuilder, private provider: DespesaProvider,
    private providerTipos: TipoProvider,
    private toast: ToastController) {

    this.despesa = this.navParams.data.despesa || { };
    this.tipos = this.providerTipos.getAll();

    this.createForm();

    this.setupPageTitle();
  }

  ionViewDidLoad(){
    console.log(this.tipos);
  }

  private setupPageTitle() {
    this.title = this.navParams.data.despesa ? 'Alterando Despesa' : 'Nova Despesa';
  }

  createForm() {
    this.form = this.formBuilder.group({
      key: [this.despesa.key],
      tipo: [this.despesa.tipo, Validators.required],
    });
  }

  onSubmit() {
    if (this.form.valid) {
      this.provider.save(this.form.value)
        .then(() => {
          this.toast.create({ message: 'Despesa salva com sucesso.', duration: 3000 }).present();
          this.navCtrl.pop();
        })
        .catch((e) => {
          this.toast.create({ message: 'Erro ao salvar a Despesa.', duration: 3000 }).present();
          console.error(e);
        })
    }
  }
}
