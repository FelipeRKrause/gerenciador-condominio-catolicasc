import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DespesaAddPage } from './despesa-add';

@NgModule({
  declarations: [
    DespesaAddPage,
  ],
  imports: [
    IonicPageModule.forChild(DespesaAddPage),
  ],
})
export class DespesaAddPageModule {}
