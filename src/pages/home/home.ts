import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,
    private toast: ToastController) {

  }

  openPushMoradores(): void{
    this.navCtrl.push('ContactHomePage');
  }

  openPushTipos(): void{
    this.navCtrl.push('TipoHomePage');
  }

  openPushFornecedores(): void{
    this.navCtrl.push('FornecedorPage');
  }

  openPushDespesas(): void{
    this.navCtrl.push('DespesaAddPage');
  }

}
