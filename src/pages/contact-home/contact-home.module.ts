import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactHomePage } from './contact-home';

@NgModule({
  declarations: [
    ContactHomePage,
  ],
  imports: [
    IonicPageModule.forChild(ContactHomePage),
  ],
})
export class ContactHomePageModule {}
