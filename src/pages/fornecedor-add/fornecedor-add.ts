import { FornecedorProvider } from './../../providers/fornecedor/fornecedor';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-fornecedor-add',
  templateUrl: 'fornecedor-add.html',
})
export class FornecedorAddPage {
  title: string;
  form: FormGroup;
  fornecedor: any;

  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    private formBuilder: FormBuilder, private provider: FornecedorProvider,
    private toast: ToastController) {

    // maneira 1
    this.fornecedor = this.navParams.data.fornecedor || { };
    this.createForm();

    this.setupPageTitle();
  }

  private setupPageTitle() {
    this.title = this.navParams.data.fornecedor ? 'Alterando Fornecedor' : 'Novo Fornecedor';
  }

  createForm() {
    this.form = this.formBuilder.group({
      key: [this.fornecedor.key],
      name: [this.fornecedor.name, Validators.required],
      cpfcnpj: [this.fornecedor.cpfcnpj, Validators.required],
      tel: [this.fornecedor.tel, Validators.required],
      servicos: [this.fornecedor.servicos, Validators.required],
    });
  }

  onSubmit() {
    if (this.form.valid) {
      this.provider.save(this.form.value)
        .then(() => {
          this.toast.create({ message: 'Fornecedor salvo com sucesso.', duration: 3000 }).present();
          this.navCtrl.pop();
        })
        .catch((e) => {
          this.toast.create({ message: 'Erro ao salvar o Fornecedor.', duration: 3000 }).present();
          console.error(e);
        })
    }
  }
}
