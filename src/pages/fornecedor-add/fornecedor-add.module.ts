import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FornecedorAddPage } from './fornecedor-add';

@NgModule({
  declarations: [
    FornecedorAddPage,
  ],
  imports: [
    IonicPageModule.forChild(FornecedorAddPage),
  ],
})
export class FornecedorAddPageModule {}
