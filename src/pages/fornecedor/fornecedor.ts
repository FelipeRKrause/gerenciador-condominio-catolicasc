import { FornecedorProvider } from './../../providers/fornecedor/fornecedor';
import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';


@IonicPage()
@Component({
  selector: 'page-fornecedor',
  templateUrl: 'fornecedor.html',
})
export class FornecedorPage {
  fornecedores: Observable<any>;

  constructor(public navCtrl: NavController, private provider: FornecedorProvider,
    private toast: ToastController) {

    this.fornecedores = this.provider.getAll();
  }

  newContact() {
    this.navCtrl.push('FornecedorAddPage');
  }

  editContact(fornecedor: any) {
    this.navCtrl.push('FornecedorAddPage', { fornecedor: fornecedor });
  }

  removeContact(key: string) {
    if (key) {
      this.provider.remove(key)
        .then(() => {
          this.toast.create({ message: 'Fornecedor removido sucesso.', duration: 3000 }).present();
        })
        .catch(() => {
          this.toast.create({ message: 'Erro ao remover o Fornecedor.', duration: 3000 }).present();
        });
    }
  }
}
