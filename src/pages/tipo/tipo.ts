import { TipoProvider } from './../../providers/tipo/tipo';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-tipo',
  templateUrl: 'tipo.html',
})                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
export class TipoPage {
  title: string;
  form: FormGroup;
  tipo: any;

  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    private formBuilder: FormBuilder, private provider: TipoProvider,
    private toast: ToastController) {

    // maneira 1
    this.tipo = this.navParams.data.t || { };
    this.createForm();

    this.setupPageTitle();
  }

  private setupPageTitle() {
    this.title = this.navParams.data.tipo ? 'Alterando tipo' : 'Novo tipo';
  }

  createForm() {
    this.form = this.formBuilder.group({
      key: [this.tipo.key],
      name: [this.tipo.name, Validators.required],
    });
  }

  onSubmit() {
    if (this.form.valid) {
      this.provider.save(this.form.value)
        .then(() => {
          this.toast.create({ message: 'Tipo salvo com sucesso.', duration: 3000 }).present();
          this.navCtrl.pop();
        })
        .catch((e) => {
          this.toast.create({ message: 'Erro ao salvar o Tipo.', duration: 3000 }).present();
          console.error(e);
        })
    }
  }
}
