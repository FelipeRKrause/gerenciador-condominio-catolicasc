import { TipoProvider } from './../../providers/tipo/tipo';
import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';


@IonicPage()
@Component({
  selector: 'page-tipo-home',
  templateUrl: 'tipo-home.html',
})
export class TipoHomePage {
  tipos: Observable<any>;

  constructor(public navCtrl: NavController, private provider: TipoProvider,
    private toast: ToastController) {

    this.tipos = this.provider.getAll();
  }

  newContact() {
    this.navCtrl.push('TipoPage');
  }

  editContact(tipo: any) {
    this.navCtrl.push('TipoPage', { tipo: tipo });
  }

  removeContact(key: string) {
    if (key) {
      this.provider.remove(key)
        .then(() => {
          this.toast.create({ message: 'Tipo de Despesa removida sucesso.', duration: 3000 }).present();
        })
        .catch(() => {
          this.toast.create({ message: 'Erro ao remover o tipo de Despesa.', duration: 3000 }).present();
        });
    }
  }
}
