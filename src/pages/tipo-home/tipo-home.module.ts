import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TipoHomePage } from './tipo-home';

@NgModule({
  declarations: [
    TipoHomePage,
  ],
  imports: [
    IonicPageModule.forChild(TipoHomePage),
  ],
})
export class TipoHomePageModule {}
